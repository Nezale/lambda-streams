package service;

import Domain.Person;
import Domain.Role;
import Domain.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    private static List<User> users;

    private static List<User> initUsersList(){

        User user1 = new User();
        User user2 = new User();
        User user3 = new User();

        users = Arrays.asList(user1,user2,user3);

        return users;
    }

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {

       // initUsersList();

        users.stream().filter(t->t.getPersonDetails().getAddresses().listIterator().hasNext());

        return users;
    }

    public static Person findOldestPerson(List<User> users) {

       // initUsersList();

        Comparator<User> comp = (u1,u2)-> Integer.compare(u1.getPersonDetails().getAge(),u2.getPersonDetails().getAge());

        User oldest = users.stream().max(comp).get();

        return oldest.getPersonDetails();

    }

    public static User findUserWithLongestUsername(List<User> users) {

       // initUsersList();

        Comparator<User> comp = (u1,u2)-> Integer.compare(u1.getName().length(),u2.getName().length());

        User longest = users.stream().max(comp).get();

        return longest;

    }

    public static void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

       // initUsersList();

        users.stream().filter(p->p.getPersonDetails().getAge()>=18).forEach(p->System.out.println(p.getPersonDetails().getName()+p.getPersonDetails().getSurname()));


    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {

        users.stream().filter(p->p.getPersonDetails().getName().charAt(0)=='A');

        List <String> usersA = users.stream().map(u->u.getPersonDetails().getRole().getName()).collect(Collectors.toList());

        List<String> sortedlist = usersA.stream().sorted((u1, u2)-> u1.compareTo(u2)).collect(Collectors.toList());

        return sortedlist;
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {

        users.stream().filter(p->p.getPersonDetails().getSurname().charAt(0)=='S').forEach(u->System.out.println(u.getPersonDetails().getRole().getPermissions()));

    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {



    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {

    }
}
